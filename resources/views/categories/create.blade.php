@extends('layouts/master')


@section('title')
<h1>Ajoute une Categorie</h1>
@endsection

@section('content')


<form method="POST" action="{{ route('categories.store')}}">
    @csrf

    <legend>Ajoute une Categorie</legend>

    <div class="form-group">
        <label for="name">Nom Categorie</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" >
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{$message}}</strong>
        </span>
        @enderror
      </div>
    <button type="submit" class="btn btn-primary">Envoyer</button>
  </form>
@endsection
