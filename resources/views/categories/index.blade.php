@extends('layouts/master')

@section('title')
liste des Categorie
@endsection

@section('content')
<h1>liste des Categorie</h1>



@if (session()->get('success'))
    <div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
    </div>
@endif

<table class="table">
    <thead>
      <tr>
        <th scope="col">Name</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($categories as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td><a href="{{route('categories.show',$item->id)}}" class="btn btn-sm btn-warning">Consulter</a></td>
                <td><a href="{{route('categories.edit',$item->id)}}" class="btn btn-sm btn-success">Edite</a></td>
                <td>
                    <form action="{{route('categories.destroy',$item->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
  <a href="{{route('categories.create')}}" class="btn btn-info">New category</a>
@endsection
