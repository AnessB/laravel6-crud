@extends('layouts/master')


@section('title')
Modifier une Categorie
@endsection

@section('content')
<h1>Modifier une Categorie</h1>


<form method="POST" action="{{ route('categories.update',$categorie->id)}}">

    @csrf
    @method('PATCH')
    
    <legend>Ajoute une Categorie</legend>

    <div class="form-group">
        <label for="name">Nom Categorie</label>
        <input type="text" class="form-control"  name="name"  value="{{$categorie->name}}">
      </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>


@endsection
