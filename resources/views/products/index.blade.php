@extends('layouts/master')

@section('title')
list produit
@endsection

@section('content')
<h1>list products</h1>
<a href="{{route('products.create')}}" class="btn btn-success">New product</a>

@if (session()->get('success'))
    <div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
    </div>
@endif


<h1>khalil</h1>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Price</th>
        <th scope="col">expirationDate</th>
        <th scope="col">Actions</th>
        <th scope="col">Edite</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($products as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td>{{$item->expirationDate}}</td>
                <td><a href="{{route('products.show',$item->id)}}" class="btn btn-sm btn-warning">Consulter</a></td>
                <td><a href="{{route('products.edit',$item->id)}}" class="btn btn-sm btn-warning">Edite</a></td>
                <td>
                    <form action="{{route('products.destroy',$item->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
  @endsection
