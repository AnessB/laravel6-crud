@extends('layouts/master')


@section('title')
edit product
@endsection

@section('content')
<h1>edit product</h1>



<form method="POST" action="{{ route('products.update',$product->id)}}">
    @csrf
    @method('PATCH')
  <fieldset>
    <legend>create new product</legend>

    <div class="form-group">
      <label for="name">Nom</label>
      <input type="text" class="form-control" name="name" value="{{$product->name}}">
    </div>


    <div class="form-group">
      <label for="Prix">Prix</label>
      <input type="Number" class="form-control" name="price" value="{{$product->price}}">
    </div>

    <div class="form-group">
      <label for="description">Description</label>
      <textarea class="form-control" name="description" rows="3">{{$product->description}}</textarea>
    </div>

      <div class="form-group">
        <label for="expirationDate">expirationDate</label>
        <input type="date" class="form-control" name="expirationDate" value="{{$product->expirationDate}}">
      </div>

    <button class="btn btn-primary" type="submit">Update</button>
  </fieldset>
</form>

@endsection
