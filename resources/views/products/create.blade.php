@extends('layouts/master')


@section('title')
<h1>create new product</h1>
@endsection

@section('content')
Ajout nouveau produit
<form method="POST" action="{{ route('products.store')}}">
    @csrf
  <fieldset>
    <legend>create new product</legend>

    <div class="form-group">
      <label for="name">Nom</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" >
      @error('name')
      <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
      </span>
      @enderror
    </div>


    <div class="form-group">
      <label for="Prix">Prix</label>
      <input type="Number" class="form-control @error('price') is-invalid @enderror" name="price" >
      @error('price')
      <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group">
      <label for="description">Description</label>
      <textarea class="form-control @error('description')  is-invalid @enderror" name="description" rows="3"></textarea>
      @error('description')
      <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
      </span>
      @enderror
    </div>

      <div class="form-group">
        <label for="expirationDate">expirationDate</label>
        <input type="date" class="form-control @error('expirationDate')  is-invalid @enderror" name="expirationDate" placeholder="expirationDate">
        @error('expirationDate')
      <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
      </span>
      @enderror
      </div>

    <button class="btn btn-primary" type="submit">Envoyer</button>
  </fieldset>
</form>

@endsection
