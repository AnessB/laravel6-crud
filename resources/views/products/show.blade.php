@extends('layouts/master')

@section('title')
Show products
@endsection

@section('content')
<h1>show products</h1>
<div classe="contrainer">
    <div class="card" style="width: 18rem;">
        <img src="https://images.unsplash.com/photo-1479894720049-067d8b87f2d9?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ $product->name }}</h5>
          <h6>{{ $product->price }} TND</h6>
          <p class="card-text">{{ $product->description }}</p>
          <a href="{{route('products.edit',$product->id)}}" class="btn btn-primary">Edite</a>
        </div>
      </div>
</div>
@endsection
